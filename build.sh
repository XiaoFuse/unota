#!/bin/sh
cp ${ROOT}/nota.* ${ROOT}/apparmor.json ${ROOT}/manifest.json ${INSTALL_DIR}
mkdir -p ${CLICK_PATH}
wget https://download.kde.org/stable/maui/nota/1.2.2/nota-stable-v1.2.2-arm64.AppImage -qO ${CLICK_PATH}/nota
chmod +x ${CLICK_PATH}/nota
